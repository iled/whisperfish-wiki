Whisperfish is a third-party, unofficial client for [Signal](https://signal.org/), the private messenger, for SailfishOS.  It is available on [OpenRepos](https://openrepos.net/node/11046/), or as [master-branch builds on Gitlab](https://gitlab.com/rubdos/whisperfish/).

You have a question? Please check our **[Frequently Asked Questions](Frequenty-Asked-Questions)**. If the FAQ does not answer your question, join us on [Matrix or Freenode](Contact-Us), make [an issue on Gitlab](https://gitlab.com/rubdos/whisperfish/-/issues) or [send me an email, Signal or text message](https://www.rubdos.be/about/)!

If you want to edit this wiki, you can do so through [this mirror repository](https://gitlab.com/rubdos/whisperfish-wiki/).

## Notably unimplemented features

- [Contact discovery and sharing](https://gitlab.com/rubdos/whisperfish/-/issues/133), see also https://github.com/Michael-F-Bryan/libsignal-service-rs/pull/52.
- [GroupV2](https://gitlab.com/rubdos/whisperfish/-/issues/86)
- [UUID-only devices](https://gitlab.com/rubdos/whisperfish/-/issues/80),
    although these do not exist yet (I think).
- [Group management](https://gitlab.com/rubdos/whisperfish/-/issues/100) (creating and modifying groups)
- [Linking devices](https://gitlab.com/rubdos/whisperfish/-/issues/97) (i.e., Signal Desktop)
- [Session reset](https://gitlab.com/rubdos/whisperfish/-/issues/114) and [fingerprint changes](https://gitlab.com/rubdos/whisperfish/-/issues/121)
