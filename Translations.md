![Whisperfist translation status](https://hosted.weblate.org/widgets/whisperfish/-/whisperfish-application/287x66-grey.png)

Whisperfish exists in a few languages already:

- 🇩🇪 German (`de`), by Stephan Lohse
- 🇪🇸 Spanish (`es`), by `carlosgonz`
- 🇫🇮 Finnish (`fi`), by Markus `entil` Törnqvist
- 🇫🇷 French (`fr`), by Thibaut Vandervelden
- 🇭🇺 Hungarian (`hu`), by `1Zgp`
- 🇳🇱 Dutch (`nl`) and 🇧🇪 Belgian 🏴‍☠️ Arrrrgh Dutch (`nl_BE`) by Ruben `rubdos` De Smet and Nathan `nthn`.
- 🇳🇴 Norse (`no_NO`), by Allan `kingu` Nordhøy
- 🇵🇱 Polish (`pl`), by `paytchoo`
- 🇵🇹 Portuguese (`pt_PT`), by Júlio `iled`
- 🇹🇷 Turkish (`tr`), by Oğuz `ersen` Ersen
- 🇨🇳 Simplified Chinese (`zh_CN`), by `dashinfantry`

If you translated something, please [update this list](https://gitlab.com/rubdos/whisperfish-wiki) or let me know!
These credits are really important to me 😄!
I'm very grateful for this good work!

Additionally, there's the small contributions of single words by [many people
on Weblate](https://hosted.weblate.org/projects/whisperfish/whisperfish-application/#history).
The translations there are moving really fast, faster than we can update the
above list.  Thank you all who contributed!

Translations are never complete, especially since Whisperfish is moving very
fast.  Our progress can be seen here:

[![Whisperfish translation progress](https://hosted.weblate.org/widgets/whisperfish/-/whisperfish-application/multi-auto.svg)](https://hosted.weblate.org/engage/whisperfish/)

## How to update translations

There are two ways to contribute translations.

### Via Weblate

We created a [Weblate page](https://hosted.weblate.org/engage/whisperfish/).
On that page, you can easily alter the translations via a web interface.
These changes are then pushed by Weblate to our repository here.

### Manually, with Git

If you prefer, you can manually edit the `.ts` files, or use the software of
your choice to edit the Qt linguist files.
Changing translations strings is pretty simple:

1. Get in the repository
2. Find your language in `translations/`, open the respective `.ts` file.
3. Find the translatable string and change the contents between `<translation>` and `</translation>`.  If the starting tag is `<translation type="unfinished">`, change it to `<translation>`.
4. Save the file, commit (with a message saying you updated the translation), and make a merge request.

Additionally, you may want to look specifically for strings with `type="unfinished"` (by using your text editor to search for "unfinished"), because those strings need to be looked after.  Strings tagged with `type="unfinished"` have recently been touched in the application itself, may have changed their meaning slightly, or are entirely new.

Qt Linguist is also an application with a GUI to facilitate translations. Essentially, using it will replace step `3.` above. You can get Qt Linguist from the [Qt bundle](https://www.qt.io/download) or you can just download the [standalone app](https://github.com/lelegard/qtlinguist-installers). Open the `.ts` file from within the app, then it will automatically detect and list the strings that need to be translated.

## Translate Whisperfish a new language

If you would like to contribute a translation for a new language,
you'll need a new translation file.

1. First, you need to know your language identifier.
   Examples are `nl_BE`, `nl`, `de`, or `zh_CN`.

2. Generate the translation file, execute the following command in the
   Whisperfish source's root directory:
```sh
lupdate qml/ -noobsolete -ts translations/harbour-whisperfish-$LANG.ts
```

This should generate a file `translations/harbour-whisperfish-$LANG.ts`.
If you do not have the `lupdate` utility, [get in touch with
Ruben](Contact-Us), he'll create your file.

**Important**: please create a merge request *as soon* as you create your new file, before starting to translate. We've had it happen that two people write a complete translation from scratch one day apart.

## About updating the QML files or English source material.

If you want to update the English translation,
or when you make an update to the QML files,
you'll need to update *all* the translation files,
because they refer to specific line numbers in the source files.
You can run `./update-translations.sh` in the source directory for this.

You also need to run this script when the source material (English source
translation) changes;
this will update the source strings in the language files *and* will invalidate
the translated strings.

You can only alter the English source language *in the QML files*.
This is an artifact of how Qt Linguist seems works.
