## Can I migrate from Signal Android to Whisperfish?

**Yes**, under a few conditions:

1. You do *not* have any [V2 groups](https://gitlab.com/rubdos/whisperfish/-/issues/86).
2. You don't mind that Whisperfish will not [import any Android messages](https://gitlab.com/rubdos/whisperfish/-/issues/15), specifically, Whisperfish will be a *new registration*.
3. You need to turn-off the registration lock in the Android Signal settings. Cfr. https://gitlab.com/rubdos/whisperfish/-/issues/148 and https://gitlab.com/rubdos/whisperfish/-/issues/149

## I have heard Signal bans people that use Whisperfish

The people behind Signal have been quite outspoken against third-party clients in the past (see e.g. [this May 2016 comment](https://github.com/LibreSignal/LibreSignal/issues/37#issuecomment-217211165)).  That said, they seem to have changed their mind shorty after, and seem to *tolerate* us, but *ignore* most (all?) outreach.

I, Ruben, have been quite open about the existence of Whisperfish.  We have our own very specific `User-Agent` (even including a version number) when talking to the OWS servers.  I have made our existence clear on their Github and on Reddit.  Although we have yet to hear any feedback, we tend to believe that they tolerate our presence.

This does *not* take away that they can ban any of us, when deemed necessary.  I kindly invite mr. Marlinspike and his colleagues for a friendly chat on Matrix, Freenode, or - of course - Signal!

I'd like to put things in perspective here: WhatsApp has a *whitelist* on client `User-Agent`s.
People that develop third-party clients for WhatsApp have to do this in secret, while I do all of this with my name tag present.
Signal can trivially implement a whitelist too, and it seems like a deliberate choice of Signal *not to do this*.
That said, they have the ability and most probably even the right to ban us at any given notice.

## How can I help or contribute?

There are *many* ways how you can contribute!

1. Report issues, request features, preferably [an issue on Gitlab](https://gitlab.com/rubdos/whisperfish/-/issues),
but feel free to come on [Matrix or Freenode](Contact-Us),
or [send me an email, Signal or text message](https://www.rubdos.be/about/) too.
2. Translations! ![https://hosted.weblate.org/engage/whisperfish/](https://hosted.weblate.org/widgets/whisperfish/-/whisperfish-application/svg-badge.svg) Have a look at our [translations guide](Translations), and [get in
   touch](Contact-Us) if you need help.
   The badges on our repository show how far we have translated already!
3. Writing code.  If you attempt to compile, come have a chat via [Matrix or Freenode](Contact-Us), it's not a simple undertaking.
