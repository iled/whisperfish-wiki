The main developer of Whisperfish is Ruben De Smet.
You can [contact me directly](https://www.rubdos.be/about/) if you really like,
but there are other ways to get in touch.

## Matrix

[Matrix](https://matrix.org/) is a federated network of chat rooms.
You need an account on some server in order to use it.
Find us on [#whisperfish:rubdos.be](https://matrix.to/#/#whisperfish:rubdos.be), when you have an account!
The Matrix room is bridged to Freenode (see below).

## Freenode's IRC

Find `rubdos` on Freenode, either in the SailfishOS channel, or the
[#whisperfish](https://webchat.freenode.net/#whisperfish) channel.
